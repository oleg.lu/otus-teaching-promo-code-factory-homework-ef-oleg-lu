﻿using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class RoleModel : BaseEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}

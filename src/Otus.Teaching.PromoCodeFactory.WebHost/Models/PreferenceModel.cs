﻿using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PreferenceModel : BaseEntity
    {
        public string Name { get; set; }
    }
}

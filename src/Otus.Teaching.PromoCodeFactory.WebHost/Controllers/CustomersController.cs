﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _repository;

        public CustomersController(IRepository<Customer> repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Получить всех Customers из базы данных
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            Customer customer = (await _repository.GetAllAsync()).FirstOrDefault();

            if (customer == null)
                return NotFound();

            CustomerShortResponse response = new CustomerShortResponse
            { Id = customer.Id, FirstName = customer.FirstName, LastName = customer.LastName, Email = customer.Email };

            return Ok(response);

        }
        /// <summary>
        /// Получить Customer по id из базы данных
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            Customer customer = await this._repository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            CustomerResponse response = new CustomerResponse
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
                PromoCodes = customer.PromoCode == null ? new List<PromoCodeShortResponse>()
                : new List<PromoCodeShortResponse>()
                {
                    new PromoCodeShortResponse
                    {
                        Id = customer.PromoCode.Id,
                        BeginDate = customer.PromoCode.BeginDate.ToString(),
                        EndDate = customer.PromoCode.EndDate.ToString(),
                        Code = customer.PromoCode.Code,
                        PartnerName = customer.PromoCode.PartnerName,
                        ServiceInfo = customer.PromoCode.ServiceInfo
                    }
                }
            };

            return Ok(response);

        }
        /// <summary>
        /// Добавление нового Customers
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            Customer customer = new Customer
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
            };

            Guid guid = await _repository.AddNewCustomer(customer, request.PreferenceIds);

            return Ok(guid);

        }
        /// <summary>
        /// Редактирование Customer из бд
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            Customer customer = new Customer
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
            };

            Customer editCustomer = (await _repository.EditCustomer(id, customer, request.PreferenceIds));

            if (editCustomer == null)
                return NotFound();

            return Ok(editCustomer);

        }
        /// <summary>
        /// Удаление Customer
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            bool isSuccess = await _repository.DeleteCustomer(id);

            if (!isSuccess)
                return NotFound(id);

            return Ok("Пользователь с id = " + id + " удален.");

        }
    }
}
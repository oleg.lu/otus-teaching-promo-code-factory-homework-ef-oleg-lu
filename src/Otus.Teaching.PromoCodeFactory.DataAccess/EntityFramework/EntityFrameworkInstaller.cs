﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EntityFramework
{
    public static class EntityFrameworkInstaller
    {
        public static IServiceCollection ConfigureContext(this IServiceCollection services,
            string connectionString)
        {
            DbContextOptionsBuilder<DatabaseContext> optionsBuilder =
                new DbContextOptionsBuilder<DatabaseContext>();

            DbContextOptions<DatabaseContext> options =
                optionsBuilder.UseSqlite(connectionString).Options;

            services.AddSingleton(typeof(DbContext), new DatabaseContext(options));

            return services;

        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EntityFramework
{
    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        private DatabaseContext _dbContext;
        public EfRepository(DbContext dbContext)
        {
            _dbContext = dbContext as DatabaseContext;
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _dbContext.Set<T>().ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await _dbContext.Set<T>().FindAsync(id);
        }

        public Task<Guid> AddNewCustomer(Customer customer, List<Guid> preferenceIds)
        {
            Customer addedCustomer = _dbContext.Customers.Add(customer).Entity;

            IEnumerable<Preference> preferences = _dbContext.Preferences.Where(item => preferenceIds.Contains(item.Id));

            foreach (Guid preferenceId in preferenceIds)
                _dbContext.CustomerPreferences.Add(new CustomerPreference { CustomerId = addedCustomer.Id, PreferenceId = preferenceId });

            _dbContext.SaveChanges();

            return Task.FromResult(addedCustomer.Id);
        }


        public Task<T> EditCustomer(Guid id, Customer customer, List<Guid> preferenceIds)
        {
            Customer editedCustomer = _dbContext.Customers.FirstOrDefault(item => item.Id.Equals(id));

            if (editedCustomer == null)
                return Task.FromResult(editedCustomer as T);

            editedCustomer.FirstName = customer.FirstName;
            editedCustomer.LastName = customer.LastName;
            editedCustomer.Email = customer.Email;

            foreach (Guid preferenceId in preferenceIds)
                _dbContext.CustomerPreferences.Add(new CustomerPreference { CustomerId = editedCustomer.Id, PreferenceId = preferenceId });

            _dbContext.SaveChanges();

            return Task.FromResult(editedCustomer as T);
        }


        Task<bool> IRepository<T>.DeleteCustomer(Guid id)
        {
            Customer editedCustomer = _dbContext.Customers.FirstOrDefault(item => item.Id.Equals(id));

            if (editedCustomer == null)
                return Task.FromResult(false);

           _dbContext.Remove(editedCustomer);
           _dbContext.SaveChanges();

            return Task.FromResult(true);
        }


        public Task<IEnumerable<T>> GetAllPreferences()
        {
            throw new NotImplementedException();
        }

        public Task<T> GetPreferenceById(Guid id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<T>> GetAllPromocodes()
        {
            throw new NotImplementedException();
        }

        public Task<T> AddNewPromocode(PromoCode promoCode)
        {
            throw new NotImplementedException();
        }
    }
}

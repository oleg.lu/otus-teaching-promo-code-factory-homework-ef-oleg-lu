﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Linq;
using System.Reflection.Emit;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EntityFramework
{
    public class DatabaseContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }
        public DbSet<CustomerPreference> CustomerPreferences { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }
        public DbSet<Role> Roles { get; set; }



        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
            Database.EnsureDeleted();
            Database.EnsureCreated();
            Seed();

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<CustomerPreference>().HasKey(sc => new { sc.CustomerId, sc.PreferenceId });

            modelBuilder.Entity<CustomerPreference>()
                .HasOne<Customer>(cp => cp.Customer)
                .WithMany(c => c.CustomerPreferences)
                .HasForeignKey(cp => cp.CustomerId);

            modelBuilder.Entity<CustomerPreference>()
                .HasOne<Preference>(cp => cp.Preference)
                .WithMany(p => p.CustomerPreferences)
                .HasForeignKey(cp => cp.PreferenceId);

            //modelBuilder.Entity<Customer>()
            //    .HasOne(c => c.PromoCode)
            //    .WithOne(p => p.Customer)
            //    .HasForeignKey<PromoCode>(p => p.CustomerId);

            modelBuilder.Entity<Customer>().Property(c => c.FirstName).HasMaxLength(100);
            modelBuilder.Entity<Customer>().Property(c => c.LastName).HasMaxLength(100);
            modelBuilder.Entity<Customer>().Property(c => c.Email).HasMaxLength(100);

        }

        private void Seed()
        {
            Employees.AddRange(FakeDataFactory.Employees);
            Roles.AddRange(FakeDataFactory.Roles.Where(item =>
                 !FakeDataFactory.Employees.Select(empl => empl.Role.Id).Contains(item.Id)));
            Preferences.AddRange(FakeDataFactory.Preferences);
            Customers.AddRange(FakeDataFactory.Customers);
            PromoCodes.AddRange(FakeDataFactory.PromoCodes);
            CustomerPreferences.AddRange(FakeDataFactory.CustomerPreferences);

            SaveChanges();
        }

    }
}

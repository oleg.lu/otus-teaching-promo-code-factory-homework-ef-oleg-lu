﻿using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        :BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }
        public PromoCode PromoCode { get; set; }

        public IEnumerable<CustomerPreference> CustomerPreferences { get; set; }

    }
}